import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../user'
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User;
  userParam = "";

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  searchUser(): void{
    this.userService.getUser(this.userParam)
    .subscribe((data: User) => this.user = data,
    error => console.log(error));
  }
}
