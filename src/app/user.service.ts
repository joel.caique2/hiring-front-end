import { Injectable } from '@angular/core';
import { User } from './user';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';


@Injectable()
export class UserService {

  private url = 'https://api.github.com/users/';
  constructor(private _http:Http) {}

  getUser(user: string){
    return this._http.get(this.url+user)
    .map((response: Response) => <User>response.json())
    .catch(this.handleError)
  }

  private handleError(error: Response) {
    console.error(error);
      return Observable.throw(error.json().error || 'Server error');
    }
}
